:: Clean up after finished working for the day.
:: This script:
::      1. Copies all fragment sources to your repository
::      2. Deletes all compiled scripts and fragments from the game directory
::      3. Deletes all fragment sources from the game directory
::
:: The goal is that when I'm done modding for the day, I can run this and it will copy the work I've done to
:: a designated git repository (if it's not already there) and then reset my skyrim directory back to it's
:: pre-work state for play.
::
:: To use:
::      1. Must have a repository created with the following structure:
::          <Root>\
::                 data\
::                      scripts\
::                              source\
::          FileManifest.txt
::
::     2. Invoke this bat file from the command line with "skclean <Repository>" where Repository is the 
::        root directory name for the repo. Do not use the full file path. (ex: skclean MyRepo)
::
:: !!! IMPORTANT !!!:
::      Scripts must begin with your script prefix and fragments must begin with your fragment prefix. If they do not, skclean will not copy them over to the repository or delete them.
@echo off
set Skyrim=C:\Program Files (x86)\Steam\SteamApps\common\Skyrim Special Edition
set dataDir=%Skyrim%\Data
set scriptDir=%Skyrim%\Data\scripts\
:: Set this to the root directory where you keep your repositories!
set repoRoot=C:\Repositories\
:: Set this to your script prefix
set scriptPrefix=_exso_
:: Set this to your fragment prefix
set fragmentPrefix=_exf_

set argC=0
:: Count the number of args
for %%x in (%*) do (
    set /A argC+=1
    set modName=%%x
    set target_repo=%repoRoot%%%x
)

:: Check that we've passed in the correct number of args
IF NOT %argC%==1 (
    echo "Not Enough Args - %argC%"
    exit /b
)

:: Check if we're dealing with an actual repository
IF NOT EXIST %target_repo% (
    echo "ERROR: %target_repo% not found in repositories"
    exit /b
)

:: Check that a File Manifest (FileManifest.txt) exists in the repository. If not, create one.
IF NOT EXIST %target_repo%\FileManifest.txt (
    echo WARN: %target_repo%\FileManifest.txt does not exist. Creating it now.
    echo. 2>%target_repo%\FileManifest.txt
)

:: Copy fragments to repo. Scripts are already there so we don't need to do them
xcopy /s /y "%dataDir%\source\scripts\_exf_*.psc" %target_repo%\data\scripts\source 2>NUL

:: Clean compiled script and fragment artifacts
:: Compiled scripts
del "%scriptDir%\%scriptPrefix%*.pex" 2>NUL
:: Compiled fragments
del "%scriptDir%\%fragmentPrefix*.pex" 2>NUL
:: Fragment sources
del "%dataDir%\source\scripts\%fragmentPrefix%*.psc" 2>NUL

:: Get the Repository File Manifest and move the expected files over
for /f %%a in (%target_repo%\FileManifest.txt) do (
    xcopy /s /y "%dataDir%\%%a" %target_repo%\data
)