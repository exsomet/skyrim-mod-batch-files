:: Compile scripts for the mod
:: This script:
::      1. Copies fragment sources to the target repository
::      2. Invokes the Papyrus Compiler against all scripts and fragments in the repository using ScriptCompile.bat Compiled scripts
::         will be saved to the game directory.
::
:: This script doesn't move the esm/esp over because once it is completed the esm/esp will be outdated and will need to be manually saved
::
:: To use:
::      1. Must have a repository created with the following structure:
::          <Root>\
::                 data\
::                      scripts\
::                              source\
::          FileManifest.txt
::      
::      2. Scripts must be placed in source\. Fragments will be copied there automatically.
::      3. Invoke this bat file from the command line with "skbuild <Repository>" where Repository is the 
::        root directory name for the repo. Do not use the full file path. (ex: skbuild MyRepo)
::
:: !!! IMPORTANT !!!:
::      Scripts must begin with _exso_ and fragments must begin with _exf_. If they do not, skbuild will not copy them over to the repository.
@echo off
set Skyrim=C:\Program Files (x86)\Steam\SteamApps\common\Skyrim Special Edition\
set scriptDir=%Skyrim%Data\scripts\
set workingDir=%cd%
:: Set this to the root directory where you keep your repositories!
set repoRoot=C:\Repositories\
:: Set this to your script prefix
set scriptPrefix=_exso_
:: Set this to your fragment prefix
set fragmentPrefix=_exf_

:: Check that we've passed in the correct args
set argC=0
for %%x in (%*) do (
    set /A argC+=1
    set target_repo=%repoRoot%%%x
)
IF NOT %argC%==1 (
    echo "Not Enough Args - %argC%"
    exit /b
)

:: Check if we're dealing with an actual repository
IF NOT EXIST %target_repo% (
    echo "ERROR: %target_repo% not found in repositories"
    exit /b
)

echo %Skyrim%

:: Copy fragments to repo. Scripts are already there so we don't need to do them
xcopy /s /y "%Skyrim%\Data\source\scripts\%fragmentPrefix%*.psc" %target_repo%\data\scripts\source 2>NUL

:: Invoke papyrus compiler
echo Invoking Papyrus Compiler with args:
echo     Skyrim      - %Skyrim%
echo     target_repo - %target_repo%
echo -------------------------------------------------------
cd %Skyrim%\Papyrus Compiler\
call ScriptCompile.bat %target_repo%\data\scripts\source\
cd %workingDir%